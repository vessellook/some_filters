from PIL import Image
import numpy as np
from skimage.feature.texture import graycomatrix, graycoprops


def get_texture_data(image: np.ndarray):
    image = np.asarray(image)
    distances = [1, 2]
    angles = [0, np.pi / 2]
    g = graycomatrix(image, distances, angles, levels=256, normed=True, symmetric=True)
    contrast = graycoprops(g, 'contrast').flatten()
    dissimilarity = graycoprops(g, 'dissimilarity').flatten()
    homogeneity = graycoprops(g, 'homogeneity').flatten()
    energy = graycoprops(g, 'energy').flatten()
    return {
        'contrast': list(contrast),
        'dissimilarity': list(dissimilarity),
        'homogeneity': list(homogeneity),
        'energy': list(energy),
    }
