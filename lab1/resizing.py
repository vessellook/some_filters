from PIL import Image
from math import floor

M = 4
N = 3


def interpolation(img, m=M):
    """Stretching (interpolation) of the image by M times"""

    # get two sizes of the picture
    width = img.width
    height = img.height

    # set new sizes of the result picture
    new_width = floor(width * m - 1)
    new_height = floor(height * m - 1)

    # create a new picture
    new_img = Image.new("RGB", (new_width, new_height))

    for x in range(new_width):
        for y in range(new_height):
            cur_pixel = img.getpixel((x / m, y / m))
            new_img.putpixel((x, y), cur_pixel)
    return new_img


def decimation(img, n=N):
    """Image compression (decimation) by N times"""

    # get two sizes of the picture
    width = img.width
    height = img.height

    # set new sizes of the result picture
    new_width = floor(width / n - 1)
    new_height = floor(height / n - 1)

    # create a new picture
    new_img = Image.new("RGB", (new_width, new_height))
    for x in range(new_width):
        for y in range(new_height):
            cur_pixel = img.getpixel((x * n, y * n))
            new_img.putpixel((x, y), cur_pixel)
    return new_img


def resampling(img, k=M/N):
    """Resampling an image by a factor of K in one pass"""

    # get two sizes of the picture
    width = img.width
    height = img.height

    # set new sizes of the result picture
    new_w = floor(width * k - 1)
    new_h = floor(height * k - 1)

    # create a new picture
    new_img = Image.new("RGB", (new_w, new_h))
    for x in range(new_w):
        for y in range(new_h):
            cur_pixel = img.getpixel((x * (1/k), y * (1/k)))
            new_img.putpixel((x, y), cur_pixel)
    return new_img
