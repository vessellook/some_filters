from PIL import Image
from grayscale_image import convert_to_grayscale
from otsu_binarization import threshold_calculation
import numpy as np

"""
Наиболее производительным методом является метод Эйквеля, зачастую применяемый для
обработки четких или контрастных изображений. Суть данного метода заключается в том, что
изображение обрабатывается при помощи двух концентрических окон: маленького – S и большого L.
Окна имеют квадратную форму. Данные окна последовательно накладываются на изображение слева
направо, сверху вниз, используя шаг в длину стороны маленького окна S. Для большого окна L
рассчитывается порог B таким образом, чтобы пиксели можно было поделить на два кластера. 
Если математические ожидания уровня яркости в двух кластерах имеют разницу, превышающую
некоторый заданный пользователем уровень /μ1-μ2/≥l, то все пиксели внутри окна S бинаризуются в
соответствии с порогом t. В противном случае, яркость пикселей из окна S заменяется значением,
близким к исходному.
"""


def get_means(arr, threshold):
    vector_arr = np.reshape(arr, arr.size)
    data_above_t = vector_arr[vector_arr >= threshold]
    data_under_t = vector_arr[vector_arr < threshold]
    # this function finishes with an error if the array is empty
    mean_above_t = np.mean(data_above_t) if data_above_t.size > 0 else 0
    mean_under_t = np.mean(data_under_t) if data_under_t.size > 0 else 0
    return mean_above_t, mean_under_t


def eikvel_binarization_helper(x, y, r_step, R_step, old_img, new_img, epsilon=15):
    # create two windows: s (small) and l (large)
    s_window = old_img[y: min(y + r_step, old_img.shape[0]),
                       x: min(x + r_step, old_img.shape[1])]
    l_window = old_img[max(y - R_step // 2 + 1, 0): min(y + R_step // 2 + r_step - 1, old_img.shape[0]),
                       max(x - R_step // 2 + 1, 0): min(x + R_step // 2 + r_step - 1, old_img.shape[1])]

    # calculate the otsy-threshold for the large window
    t = threshold_calculation(l_window)
    # and then calculate means values
    mean_above_t, mean_under_t = get_means(l_window, t)

    # check the condition with epsilon=15
    if abs(mean_above_t - mean_under_t) >= epsilon:
        # inside the inner window the pixels are binarized according to the threshold "t"
        new_img[y: min(y + r_step, old_img.shape[0]),
                x: min(x + r_step, old_img.shape[1])][s_window > t] = 255
    else:
        # calculate the center of the small window
        center = s_window[s_window.shape[0] // 2, s_window.shape[1] // 2]

        if abs(mean_above_t - center) < abs(mean_under_t - center):
            # brightness of pixels from the small window is replaced with white
            new_img[y: min(y + r_step, old_img.shape[0]),
                    x: min(x + r_step, old_img.shape[1])] = 255


def eikvel_binarization(old_image, epsilon, r_step, R_step):
    if r_step % 2 == 0 or R_step % 2 == 0:
        raise ValueError("You can input only even sizes of the windows!")

    grayscale_img = convert_to_grayscale(old_image)
    arr_grayscale_img = np.array(grayscale_img)

    new_img = np.zeros(shape=arr_grayscale_img.shape)

    x, y = 0, 0
    while y + r_step <= arr_grayscale_img.shape[0]:
        if y % 2 == 0:
            while x + r_step < arr_grayscale_img.shape[1]:
                eikvel_binarization_helper(x, y, r_step, R_step, arr_grayscale_img, new_img, epsilon)
                x += r_step
        else:
            while x - r_step > 0:
                eikvel_binarization_helper(x, y, r_step, R_step, arr_grayscale_img, new_img, epsilon)
                x -= r_step
        y += r_step

    return Image.fromarray(new_img.astype(np.uint8), 'L')
