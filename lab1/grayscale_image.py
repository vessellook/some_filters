from PIL import Image

"""
From StackOverflow:
Если у вас есть L изображение режима, это означает, что это одноканальное изображение, 
обычно интерпретируемое как оттенки серого. 
L означает, что просто хранит Luminance. 
Он очень компактен, но сохраняет только оттенки серого, а не цвет.
"""


def convert_to_grayscale(img, mode="photoshop"):
    """Converting a Full Color Image to a Grayscale Image"""

    # Check whether the picture is a Grayscale or not
    if img.mode == "L":
        return img

    # get two sizes of the picture
    width = img.width
    height = img.height

    # create a new grayscale picture depending on the chosen mode
    new_img = Image.new("L", (width, height))
    for x in range(width):
        for y in range(height):
            pixel = img.getpixel((x, y))
            sum_pix = 0
            if mode == "usual":
                sum_pix = sum(pixel) // 3
            elif mode == "photoshop":
                sum_pix = 0.3 * pixel[0] + 0.59 * pixel[1] + 0.11 * pixel[2]
            else:
                raise NameError("There are only two modes of converting! ('usual' and 'photoshop')")
            new_img.putpixel((x, y), int(sum_pix))
    return new_img
