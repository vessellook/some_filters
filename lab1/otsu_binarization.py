from PIL import Image
import numpy as np

from lab1.grayscale_image import convert_to_grayscale


def threshold_calculation(arr_img):
    """Calculating the threshold for the Otsy binarization method"""

    bins = np.arange(arr_img.min() - 1, arr_img.max() + 1)

    # use this function to calculate the relative frequencies w for the histogram zones to the left and right of t
    p, indexes = np.histogram(arr_img, bins=bins, density=True)

    indexes = indexes[1:].astype(np.uint8)

    # indexes = indexes[1:].astype(np.uint8)
    w0 = np.cumsum(p)
    ones = np.ones(shape=w0.shape)
    w1 = ones - w0

    t = 0
    max_i = 0
    for i, (w0_i, w1_i) in enumerate(zip(w0, w1)):
        # calculate the average levels M of brightness in each zone:
        m0, m1 = np.sum(indexes[: i] * p[: i] / w0_i), np.sum(indexes[1 + i:] * p[1 + i:] / w1_i)

        # calculate the classes variances D:
        d0, d1 = np.sum(p[: i] * (indexes[: i] - m0) ** 2), np.sum(p[i + 1:] * (indexes[i + 1:] - m1) ** 2)

        # calculate the d_common:
        d_common = w0_i * d0 + w1_i * d1
        # calculate the d_class:
        d_class = w0_i * w1_i * (m0 - m1) ** 2

        # choose the max values (max_i and t)
        if d_common == 0:
            max_i = i
            break
        current_t = d_class / d_common
        if current_t > t:
            t = current_t
            max_i = i

    return indexes[max_i]


# since I calculated the threshold, why not calculate the binarization
def otsy_binarization(img, need=True):
    arr_grayscale_img = None

    if need:
        arr_grayscale_img = np.array(convert_to_grayscale(img))
    else:
        arr_grayscale_img = np.array(img)

    threshold = threshold_calculation(arr_grayscale_img)  # threshold

    # create a new picture
    new_img = np.zeros(shape=arr_grayscale_img.shape)
    for i in range(new_img.shape[0]):
        for j in range(new_img.shape[1]):
            # if more than threshold then coloring in white else in stay in black
            new_img[i][j] = 255 if (arr_grayscale_img[i][j] > threshold) else 0

    return Image.fromarray(new_img.astype(np.uint8), 'L')
