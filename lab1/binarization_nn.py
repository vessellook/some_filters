from PIL import Image
import numpy as np
import tensorflow as tf
from keras_unet.metrics import iou, iou_thresholded

from lab1.imsplit import imsplit, replace_segments, imjoin_average
from lab1.otsu_binarization import otsy_binarization
from application.settings import MODEL_PATH

custom_objects = dict(iou_thresholded=iou_thresholded, iou=iou)


def to_color(gray: np.ndarray):
    return np.repeat(np.expand_dims(gray, axis=2), 3, axis=2)


def apply(image: Image.Image):
    return process(np.array(image), str(MODEL_PATH))


def process(image: np.ndarray, model_path):
    model = tf.keras.models.load_model(model_path, custom_objects=custom_objects)
    if image.ndim == 2:
        image = to_color(image)
    image = image / 255
    crops = imsplit(image, 256, step=64)
    batch = np.array([crop[0] for crop in crops])
    result_batch = model.predict(batch)
    result_crops = replace_segments(crops=crops, new_segments=result_batch)
    result_image = imjoin_average(result_crops)
    result_image = np.squeeze((result_image * 255).astype(np.uint8))
    return otsy_binarization(Image.fromarray(result_image))
