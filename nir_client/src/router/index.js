import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  {
    path: "/",
    redirect: "/images/",
  },
  {
    path: "/images/",
    name: "images",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ImagesList.vue"),
  },
  {
    path: "/images-markup/:imgId",
    name: "markup",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ImagesMarkup.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
