import { createStore } from "vuex";

export default createStore({
  state: {
    isShownAddition: false,
    isShownModal: false,
    temporarilyFilter: "",
    imageFilters: [
      {
        filter: "Адаптивный медианный фильтр", // либо null, либо строка-название
        isChosen: false,
        letter: "A",
      },
      {
        filter: "Фильтр Баттерворта 50", // либо null, либо строка-название
        isChosen: false,
        letter: "B",
      },
      {
        filter: "Фильтр Баттерворта 80", // либо null, либо строка-название
        isChosen: false,
        letter: "M",
      },
      {
        filter: "Фильтр Баттерворта 70", // либо null, либо строка-название
        isChosen: false,
        letter: "N",
      },
      {
        filter: "Фильтр Баттерворта 60", // либо null, либо строка-название
        isChosen: false,
        letter: "O",
      },
      {
        filter: "Фильтр Баттерворта 40", // либо null, либо строка-название
        isChosen: false,
        letter: "P",
      },
      {
        filter: "Фильтр Баттерворта 90", // либо null, либо строка-название
        isChosen: false,
        letter: "Q",
      },
      {
        filter: "Консервативный фильтр", // либо null, либо строка-название
        isChosen: true,
        letter: "C",
      },
      {
        filter: "Фильтр, основанный на морфологии", // либо null, либо строка-название
        isChosen: false,
        letter: "D",
      },
      {
        filter: "Медианный фильтр", // либо null, либо строка-название
        isChosen: false,
        letter: "E",
      },
    ],
    images: [],
  },
  getters: {},
  mutations: {
    setShownAddition(state, newShownAddition) {
      state.isShownAddition = newShownAddition;
      if (newShownAddition) {
        document.body.classList.add("show-modal");
      } else {
        document.body.classList.remove("show-modal");
      }
    },
    setShownModal(state, newShownModal) {
      state.isShownModal = newShownModal;
      if (newShownModal) {
        document.body.classList.add("show-modal");
      } else {
        document.body.classList.remove("show-modal");
      }
    },
    setTemporarilyFilter(state, newTemporarilyFilter) {
      state.temporarilyFilter = newTemporarilyFilter;
    },
    setImages(state, newImagesList) {
      state.images = newImagesList;
    },
    setFilter(state, filter) {
      state.imageFilters.map((img) => {
        img.isChosen = img.filter === filter;
      });
    },
    setRandomPicture(state) {
      state.imageFilters.map((image) => {
        image.url = `example${Math.random() > 0.5 ? 2 : 3}.jpeg`;
      });
    },
  },
  actions: {},
});
