export async function getImages() {
  const resp = await fetch(
    `http://${window.location.hostname}:8000/api/photos/`
  );
  return await resp.json();
}

export function addImages(images) {
  const formData = new FormData();
  images.forEach((image) => formData.append("files", image));
  return fetch(`http://${window.location.hostname}:8000/api/photos/`, {
    method: "POST",
    body: formData,
  });
}

export function getImageUrl(image, filter) {
  const url = new URL(image.url);
  url.searchParams.append("filter", filter);
  return url.toString();
}

export function setRatingsToImage(image, ratings) {
  const url = image.url;
  return fetch(url, {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ ratings }),
  });
}

export function deleteImage(image) {
  return fetch(image.url, {
    method: "DELETE",
  });
}
