import numpy as np
from PIL import Image, ImageOps

from lab1.binarization_nn import apply as nn_apply


def apply(img: Image.Image):
    width, height = img.size
    img = ImageOps.grayscale(img)
    img = np.array(img)
    newimg = img.copy()

    def AdaptiveMedianFilter(sMax):
        filterSize = 3
        borderSize = sMax // 2
        imgMax = img[0, 0]
        mid = (filterSize * filterSize) // 2
        for i in range(width):
            for j in range(height):
                if (imgMax < img[j, i]):
                    imgMax = img[j, i]

        for i in range(borderSize, width - borderSize):
            for j in range(borderSize, height - borderSize):
                members = [imgMax] * (sMax * sMax)
                filterSize = 3
                zxy = img[j, i]
                result = zxy
                while (filterSize <= sMax):
                    borderS = filterSize // 2
                    for k in range(filterSize):
                        for t in range(filterSize):
                            members[k * filterSize + t] = img[j + t - borderS, i + k - borderS]
                            # print(members[k*filterSize+t])
                    members.sort()
                    med = (filterSize * filterSize) // 2
                    zmin = members[0]
                    zmax = members[(filterSize - 1) * (filterSize + 1)]
                    zmed = members[med]
                    if (zmed < zmax and zmed > zmin):
                        if (zxy > zmin and zxy < zmax):
                            result = zxy
                        else:
                            result = zmed
                        break
                    else:
                        filterSize += 2

                newimg[j, i] = result

    def renoiseInBorder(borderSize):
        for i in range(1, width):
            for j in range(borderSize):
                newimg[j, i] = newimg[borderSize, i]
                newimg[height - j - 1, i] = newimg[height - borderSize - 1, i]

        for j in range(height):
            for i in range(borderSize):
                newimg[j, i] = newimg[j, borderSize]
                newimg[j, width - i - 1] = newimg[j, width - borderSize - 1]
# main
    for i in range(7, 1, -2):
        img[:] = newimg
        # print(i)
        AdaptiveMedianFilter(i)
        if i == 5:
            renoiseInBorder(2)

    renoiseInBorder(1)
    newimg = Image.fromarray(newimg)
    newimg = nn_apply(newimg)
    return newimg
