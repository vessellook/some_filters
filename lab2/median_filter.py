from PIL import Image, ImageOps
import numpy as np

from lab1.grayscale_image import convert_to_grayscale
from lab1.binarization_nn import apply as nn_apply


def get_difference(resource_img: Image, result_img: Image) -> Image:
    if resource_img.width != result_img.width or result_img.height != resource_img.height:
        raise ValueError('Both pictures must have the same shape!')

    resource_img_arr = np.array(convert_to_grayscale(resource_img))
    result_img_arr = np.array(result_img)

    diff_img = np.abs(result_img_arr - resource_img_arr)

    return Image.fromarray(diff_img.astype(np.uint8), 'L')


def apply(img: Image.Image):
    img = ImageOps.grayscale(img)
    filterSize = 3
    bolderSize = filterSize // 2
    members = [(0,0)] * (filterSize*filterSize)
    width, height = img.size
    newimg = Image.new("L",(width,height),"white")
    mid = (filterSize*filterSize)//2
    for times in range(1):  # number ap filter
        for i in range(bolderSize,width-bolderSize):
            for j in range(bolderSize,height-bolderSize):
                for k in range(filterSize):
                    for t in range(filterSize):
                        members[k*filterSize+t] = img.getpixel((i+k-1,j+t-1))
                members.sort()
                newimg.putpixel((i,j),(members[mid]))

    for i in range(1,width):
        newimg.putpixel((i,0),newimg.getpixel((i,1)))

    for i in range(1,width):
        newimg.putpixel((i,height-1),newimg.getpixel((i,height-2)))

    for j in range(height):
        newimg.putpixel((0,j),newimg.getpixel((1,j)))
    for j in range(height):
        newimg.putpixel((width-1,j),newimg.getpixel((width-2,j)))

    newimg = nn_apply(newimg)
    return newimg
