import numpy as np
from PIL import Image

from lab1.grayscale_image import convert_to_grayscale
from lab1.binarization_nn import apply as nn_apply


def apply(img: Image, filter_size: int = 3) -> Image:
    grayscale_img = convert_to_grayscale(img)
    # grayscale_img.show()
    arr_grayscale_img = np.array(grayscale_img)
    temp = []
    indexer = filter_size // 2
    new_img = arr_grayscale_img.copy()
    n_row, n_col = arr_grayscale_img.shape

    for i in range(n_row):
        for j in range(n_col):
            for k in range(i - indexer, i + indexer + 1):
                for m in range(j - indexer, j + indexer + 1):
                    if (k > -1) and (k < n_row):
                        if (m > -1) and (m < n_col):
                            temp.append(arr_grayscale_img[k, m])

            temp.remove(arr_grayscale_img[i, j])
            max_value = max(temp)
            min_value = min(temp)

            if arr_grayscale_img[i, j] > max_value:
                new_img[i, j] = max_value
            elif arr_grayscale_img[i, j] < min_value:
                new_img[i, j] = min_value

            temp = []

    result = Image.fromarray(new_img.astype(np.uint8), 'L')
    result = nn_apply(result)
    return result
