# Libraries
import numpy as np
from PIL import Image, ImageOps

from lab1.binarization_nn import apply as nn_apply


def apply(img: Image.Image):
    img = ImageOps.grayscale(img)
    f = np.asarray(img)

    D0 = 60
    t = 150
    n = 3

    F = np.fft.fft2(f)
    Fshift = np.fft.fftshift(F)
    M, N = f.shape
    H = np.zeros((M, N), dtype=np.float32)
    for u in range(M):
        for v in range(N):
            D = np.sqrt((u - M / 2) ** 2 + (v - N / 2) ** 2)
            H[u, v] = 1 / (1 + ((D / D0) ** n))
    Gshift = Fshift * H
    G = np.fft.ifftshift(Gshift)
    g: np.ndarray = np.abs(np.fft.ifft2(G))
    return nn_apply(Image.fromarray(g))
    # result: np.ndarray = (g > t).astype(np.uint8) * 255
    # return Image.fromarray(result)
