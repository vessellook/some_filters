import numpy as np
import cv2

from laws_textures.feature_maps import laws_textures, LAWS_VECTORS

L5, E5, S5, W5, R5 = LAWS_VECTORS[5]


def apply(gray: np.ndarray, threshold=128, min_keypoint_size=None):
    # sift = cv2.SIFT_create()
    # keypoints = sift.detect(gray, None)
    # feature_images = laws_textures(vector_dims=5).get_features(gray, compute_fully=True).astype(np.uint8)
    feature_map_object = laws_textures(vector_dims=5)
    permutations = [
        (S5, S5),
        (E5, S5),
        (L5, S5),
        (S5, R5),
    ]
    n_permutations = len(permutations)
    feature_images = feature_map_object.compute_maps_fully(gray, permutations, n_permutations).astype(np.uint8)
    sift = cv2.SIFT_create()
    keypoints = sift.detect(feature_images, None)
    gray = np.mean(feature_images, axis=2).astype(np.uint8)
    _, mask = cv2.threshold(255 - gray, threshold, 128, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    for keypoint in keypoints:
        if min_keypoint_size is not None and min_keypoint_size > keypoint.size:
            continue
        x = int(keypoint.pt[0])
        y = int(keypoint.pt[1])
        if mask[y, x] == 128:
            cv2.floodFill(mask, None, (x, y), 255)
    _, mask = cv2.threshold(mask, 129, 255, cv2.THRESH_BINARY)
    binary = mask
    # binary = np.zeros_like(gray)
    # for i in range(feature_images.shape[2]):
    #     feature_image = feature_images[..., i]
    #     _, mask = cv2.threshold(255 - feature_image, threshold, 128, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    #     for keypoint in keypoints:
    #         if min_keypoint_size is not None and min_keypoint_size > keypoint.size:
    #             continue
    #         x= int(keypoint.pt[0])
    #         y = int(keypoint.pt[1])
    #         if mask[y, x] == 128:
    #             cv2.floodFill(mask, None, (x, y), 255)
    #     cv2.imwrite(f'/tmp/feature_image.{i}.jpg', feature_image)
    #     _, mask = cv2.threshold(mask, 129, 255, cv2.THRESH_BINARY)
    #     binary = np.bitwise_or(binary, mask)
    return binary


if __name__ == '__main__':
    # img = cv2.imread('../../images/slavcorpora.ru/rsl01006569324/original/image-007.jpg')
    img = cv2.imread('/tmp/x.jpg')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = apply(img, threshold=100)
    cv2.imwrite('/tmp/image.jpg', img)
