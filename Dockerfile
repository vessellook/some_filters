FROM python:3.10.4

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /usr/src/app
COPY requirements.txt requirements.txt
RUN pip install --default-timeout=300 tensorflow==2.9.1
RUN pip install -r ./requirements.txt
COPY . .
