Django==4.1
Pillow==9.3.0
numpy==1.23.4
opencv-python-headless
djangorestframework==3.13.1
psycopg2==2.9.3
python-magic
django-cors-headers
keras-unet==0.1.2
tensorflow==2.9.1
matplotlib
scikit-image==0.19.2

