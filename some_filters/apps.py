from django.apps import AppConfig


class SomeFiltersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'some_filters'
