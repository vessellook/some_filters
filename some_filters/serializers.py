from django.urls import reverse
from rest_framework import serializers

from some_filters import models


class PhotoSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    filter = serializers.SerializerMethodField()
    fileUrl = serializers.SerializerMethodField()

    def get_url(self, obj: models.Photo):
        return reverse('photo', kwargs={'photo_id': obj.id})

    def get_fileUrl(self, obj: models.Photo):
        return obj.file.url

    def get_filter(self, obj: models.Photo):
        if not obj.ratings:
            return None
        best_filter_name = None
        best_filter_rating = 0
        for filter_name, ratings in obj.ratings.items():
            max_number = 15 if ratings[2] == 0 else 20
            rating = sum(ratings) / max_number
            if filter_name is None or best_filter_rating < rating:
                best_filter_rating = rating
                best_filter_name = filter_name
        return best_filter_name

    class Meta:
        model = models.Photo
        fields = [
            'id',
            'url',
            'filter',
            'ratings',
        ]
