import secrets

import cv2
import magic
import numpy as np
from PIL import Image
from django.core.files import File
from django.http import HttpResponse
from rest_framework import views
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response

from some_filters.serializers import PhotoSerializer
from some_filters.models import Photo, Filter
from lab1.texture_analysis import get_texture_data


class PhotosView(views.APIView):
    parser_classes = [MultiPartParser]

    def get(self, request):
        return Response(PhotoSerializer(Photo.objects.all(), many=True).data)

    def post(self, request):
        if 'files' not in request.FILES:
            return Response(status=400)
        photos = []
        for file in request.FILES.getlist('files'):
            name = secrets.token_urlsafe(100)
            input_file = file.file
            image = cv2.imdecode(np.frombuffer(input_file.read(), np.uint8), cv2.IMREAD_GRAYSCALE)
            texture_data = get_texture_data(image)
            photo = Photo.objects.create(file=File(file=file, name=name), texture_data=texture_data)
            photos.append(photo)
            for img_filter in Filter:
                img_filter: Filter
                path = f'{photo.file.path}___{img_filter.value}'
                try:
                    img = Image.open(photo.file.path).convert(mode='RGB')
                except FileNotFoundError:
                    photo.delete()
                    return Response(status=500)
                processed_img = img_filter.apply(img)
                processed_img.save(path, 'PNG')
        return Response(PhotoSerializer(photos, many=True).data, status=201)


class PhotoView(views.APIView):
    def get(self, request, photo_id):
        photo = get_object_or_404(Photo.objects, id=photo_id)
        response = HttpResponse(content_type='image/png')
        if 'filter' not in request.query_params:
            try:
                img = Image.open(photo.file.path)
            except FileNotFoundError:
                photo.delete()
                return Response(status=500)
            img.save(response, 'PNG')
            return response
        img_filter = request.query_params['filter']
        try:
            img_filter = Filter(img_filter)
        except ValueError:
            img_filter = Filter.from_label(img_filter)
        if img_filter is None:
            return Response(status=400)
        path = f'{photo.file.path}___{img_filter.value}'
        try:
            processed_img = Image.open(path)
            processed_img.save(response, 'PNG')
            return response
        except OSError:
            try:
                img = Image.open(photo.file.path).convert(mode='RGB')
            except FileNotFoundError:
                photo.delete()
                return Response(status=500)
            processed_img = img_filter.apply(img)
            processed_img.save(path, 'PNG')
            processed_img.save(response, 'PNG')
            return response

    def put(self, request, photo_id):
        photo = get_object_or_404(Photo.objects, id=photo_id)
        ratings = request.data['ratings']
        if photo.ratings is None:
            photo.ratings = ratings
        else:
            photo.ratings.update(ratings)
        photo.save(update_fields=['ratings'])
        return Response(PhotoSerializer(photo).data, status=200)

    def delete(self, request, photo_id):
        Photo.objects.filter(id=photo_id).delete()
        return Response(status=204)
