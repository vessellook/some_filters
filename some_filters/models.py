from PIL import Image
from django.db import models

from lab2 import (
    adaptive_median_filter,
    median_filter,
    butter,
    butter2,
    butter3,
    butter4,
    butter5,
    butter6,
    f as butter_f,
    new as sobel,
    conservative_filtering,
    diffandclosing,
)


class Filter(models.TextChoices):
    ADAPTIVE_MEDIAN = 'A', 'Адаптивный медианный фильтр'
    BUTTERWORT = 'B', 'Фильтр Баттерворта 50'
    CONSERVATIVE = 'C', 'Консервативный фильтр'
    DIFF_AND_CLOSING = 'D', 'Фильтр, основанный на морфологии'
    MEDIAN = 'E', 'Медианный фильтр'
    BUTTERWORT_2 = 'M', 'Фильтр Баттерворта 80'
    BUTTERWORT_3 = 'N', 'Фильтр Баттерворта 70'
    BUTTERWORT_4 = 'O', 'Фильтр Баттерворта 60'
    BUTTERWORT_5 = 'P', 'Фильтр Баттерворта 40'
    BUTTERWORT_6 = 'Q', 'Фильтр Баттерворта 90'
    SOBEL = 'G', 'Фильтр Собеля'

    @classmethod
    def from_label(self, label: str):
        for entry in Filter:
            if entry.label == label:
                return entry

    def apply(self, img: Image.Image):
        filter_to_func = {
            Filter.ADAPTIVE_MEDIAN: adaptive_median_filter.apply,
            Filter.MEDIAN: median_filter.apply,
            Filter.SOBEL: sobel.apply,
            Filter.BUTTERWORT: butter.apply,
            Filter.BUTTERWORT_2: butter2.apply,
            Filter.BUTTERWORT_3: butter3.apply,
            Filter.BUTTERWORT_4: butter4.apply,
            Filter.BUTTERWORT_5: butter5.apply,
            Filter.BUTTERWORT_6: butter6.apply,
            Filter.DIFF_AND_CLOSING: diffandclosing.apply,
            Filter.CONSERVATIVE: conservative_filtering.apply
        }
        if self in filter_to_func:
            return filter_to_func[self](img)
        raise Exception('Missed filter in apply method')


class Photo(models.Model):
    file = models.FileField(upload_to='photos')
    ratings = models.JSONField(null=True, blank=True)
    texture_data = models.JSONField(null=True, blank=True)
