from django.urls import path

from some_filters import views


urlpatterns = [
    path('photos/', views.PhotosView.as_view(), name='photos'),
    path('photos/<int:photo_id>/', views.PhotoView.as_view(), name='photo'),
]
